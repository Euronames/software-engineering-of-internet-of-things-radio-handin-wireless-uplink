from socket import socket, AF_INET, SOCK_DGRAM
from datetime import datetime

socket = socket(AF_INET, SOCK_DGRAM)
socket.bind(("", 5000))

log = open("log.log", "a")
while True:
    message = socket.recvfrom(4096).decode("utf-8")
    elements = message.split(";")
    send_time = datetime.strptime(elements[1], "%Y/%m/%d %H:%M:%S")
    time_of_arrival = datetime.now()
    message_arrival = time_of_arrival.strftime("%Y/%m/%d %H:%M:%S")
    latency = (time_of_arrival - send_time).total_seconds

    log.write("{};{};{};{};{};{}\n".format(elements[0], elements[1], message_arrival,
                                         latency, elements[2], elements[3]))
    log.flush()