from machine import Pin, ADC, I2C, RTC
from network import WLAN, STA_IF
from socket import socket, AF_INET, SOCK_DGRAM
from bh1750 import BH1750
import utime

SSID = ""
PASSWORD = ""
IP = ""
PORT = 0000

wlan = WLAN(STA_IF)
wlan.active(True)
wlan.connect(SSID, PASSWORD)

connection = socket(AF_INET, SOCK_DGRAM)
utime.sleep(10)

sent = 0

power = Pin(2, Pin.OUT)
power.value(1)
adc = ADC(Pin(36, Pin.IN))
adc.atten(ADC.ATTN_6DB)
i2c = I2C(-1, scl=Pin(26, Pin.IN), sda=Pin(25, Pin.OUT))
light_sensor = BH1750(i2c)

def wifi_connector():
    global connection
    if connection:
        connection.close()

    connection = socket(AF_INET, SOCK_DGRAM)


def no_wifi():
    while not wlan.isconnected():
        wlan.connect(SSID, PASSWORD)
        utime.sleep(1)
    renew_connection()


def send_data(sent):
    while True:
        try:
            sent += 1
            temperature = adc.read()
            voltage_conversion=((temperature*2)/4096)
            tempTemp = ((voltage_conversion-0.5)/0.01)

            luminance = light_sensor.luminance(BH1750.CONT_HIRES_1)

            date_string = date_time_string_converter(RTC().datetime())

            connection.sendall('{};{};{};{};'.format(sent, date_string, tempTemp, luminance).encode('utf-8'))

            utime.sleep(10)
        except OSError as e:
            if e.args[0] not in [113, 104]:
                raise
            renew_connection()
            break

def date_time_string_converter(date_time):
    date_string = "{}/{}/{} {}:{}:{}".format(date_time[0], date_time[1],date_time[2], date_time[4],date_time[5], date_time[6])
    return date_string

def renew_connection():
    wifi_connector()
    while True:
        try:
            connection.connect((IP, PORT))
            send_data(sent)
            break
        except OSError as e:
            if e.args[0] == 118:
                no_wifi()
                break
            elif e.args[0] not in [113, 104]:
                raise
            wifi_connector()
            utime.sleep(1)


if (SSID != "" and PASSWORD != "" and IP != "" and PORT != 0000 ): 
    no_wifi()
else:
    print("Please setup the configurations before running")
